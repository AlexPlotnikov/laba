function [X,iter] = eqr(A,eps)
A = hess(A);
for i = 1:100000
D = diag(A);
[Q, R] = qr(A);
A1 = R*Q;
D1 = diag(A1);
if norm(D1 - D)<eps
    iter = i;
    X = sort(D1);
break
end
A = A1;
end