clc
clear all
close all
format long
n = 100;
eps = 1e-10;
X01 = 1:n;
X03 = 0.1:0.1:10;
X03(n) = n;
X04 = 0.1:0.1:10;
X04(n) = 100;
X06 = 90.1:0.1:100;
X06(1) = 1; 
X07 = [];

for i = 1:n-1
    if mod(i,20) == 0 
        X07(i) = complex(i,1);
    elseif mod(i,20) == 1 && i ~= 1
        X07(i) = complex(i,-1);
    else 
    X07(i) = i;
    end
end
X07(n) = n;



A = rand(n);

[Q,R]=qr(A);
R=R-diag(diag(R))+diag(X01);
A1=Q*R*Q';

[Q,R]=qr(A);
R=R-diag(diag(R))+diag(X03);
A3 = Q*R*Q';

[Q,R]=qr(A);
R=R-diag(diag(R))+diag(X04);
A4 = Q*R*Q';

[Q,R]=qr(A);
R=R-diag(diag(R))+diag(X06);
A6 = Q*R*Q';

[Q,R]=qr(A);
R=R-diag(diag(R))+diag(X07);
A7 = Q*R*Q';

 
S = {A1, A3, A4, A6, A7};

time = [];
flag = 'matrix';
Delta = [];
X0 = {X01,X03,X04,X06,X07};

for j = 1:5
tic
X = eqr(S{j}, eps);
time = [time toc];
Delta = [Delta norm(X - X0{j}')];
end
