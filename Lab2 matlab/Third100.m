clc
clear all
close all
format long
n = 100;

X01 = 1:n;
X03 = 0.02:0.01:n/100;
X03(n) = n;
X04 = 0.02:0.01:n/100;
X04(n) = 100;
X06 = 99.01:0.01:100;
X06(1) = 1; 
X07 = [];

for i = 1:n-1
    if mod(i,20) == 0 
        X07(i) = complex(i,1);
    elseif mod(i,20) == 1 && i ~= 1
        X07(i) = complex(i,-1);
    else 
    X07(i) = i;
    end
end
X07(n) = n;



A = rand(n);
B = rand(n);
B = B*B';

[Q,R]=qr(A);
R=R-diag(diag(R))+diag(X01);
A1=Q*R*Q';

[Q,R]=qr(A);
R=R-diag(diag(R))+diag(X03);
A3 = Q*R*Q';

[Q,R]=qr(A);
R=R-diag(diag(R))+diag(X04);
A4 = Q*R*Q';

[Q,R]=qr(A);
R=R-diag(diag(R))+diag(X06);
A6 = Q*R*Q';

[Q,R]=qr(A);
R=R-diag(diag(R))+diag(X07);
A7 = Q*R*Q';

 
S = {A1, A3, A4, A6, A7};

time = zeros(5,6);
flag = 'matrix';
Delta = zeros(5,6);
X0 = {X01,X03,X04,X06,X07};



for j = 1:5
[V,D] = eig(S{j});
norm(S{j}*V - V*D)

tic
E = eig(S{j});
time(j,1) = toc;
X = sort(E);
Delta(j,1) = norm(X0{j} - X');

tic
E = eig(S{j},B);
time(j,2) = toc;
[V,D] = eig(S{j},B);
E = diag(E);
X = S{j}*V - B*V*E;
Delta(j,2) = norm(X);

tic
[V,D] = eig(S{j});
time(j,3) = toc;
X = sort(diag(D));
Delta(j,3) = norm(X0{j} - X');

tic
[V,D] = eig(S{j},'nobalance');
time(j,4) = toc;
X = sort(diag(D));
Delta(j,4) = norm(X0{j} - X');

tic
[V,D] = eig(S{j},B);
time(j,5) = toc;
X = S{j}*V- B*V*D;
Delta(j,5) = norm(X);

tic
[V,D] = eig(S{j},B,'matrix');
time(j,6) = toc;
X = S{j}*V - B*V*D;
Delta(j,6) = norm(X);
end
