clc
clear all
close all

n = 500;
X0 = 0.02:0.01:n/100;
X0(n) = n;
A = sprand(n, n, 0.01);
[Q,R]=qr(A);
R=R-diag(diag(R))+diag(X0);
A=Q*R*Q';
A = hess(A);
A(n,n) = 1e-16;
spy(A)
[V,D] = eig(A);
norm(A*V-V*D)

tic
[V,D, flag] = eigs(A,10,'smallestabs');
toc
tic
[V,D, flag] = eigs(A,15,'smallestabs');
toc
tic
[V,D, flag] = eigs(A,10,'largestabs');
toc
tic
[V,D, flag] = eigs(A,15,'largestabs');
toc

A(n,n) = 1e-100;


tic
[V,D, flag] = eigs(A,10,'smallestabs');
toc
tic
[V,D, flag] = eigs(A,15,'smallestabs');
toc
tic
[V,D, flag] = eigs(A,10,'largestabs');
toc
tic
[V,D, flag] = eigs(A,15,'largestabs');
toc