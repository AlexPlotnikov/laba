clc
clear all
close all

n = 500;
eps = 1e-4;
X01 = 1:n;
X02 = 1:n;
X03 = 1:0.2:((n-1)*0.2 +1);
X04 = 1:0.2:((n-1)*0.2 +1);
X04(n) = n;
X05 = 1:0.2:((n-1)*0.2 +1);
X05(n) = n;
X06 = n+0.2:0.2:(n+n*0.2);
X06(1) = 1;
X07 = [];

for i = 1:n-1
    if mod(i,20) == 0 
        X07(i) = complex(i,1);
    elseif mod(i,20) == 1 && i ~= 1
        X07(i) = complex(i,-1);
    else 
    X07(i) = i;
    end
end
X07(n) = n;


A = rand(n);


[Q,R]=qr(A);
R=R-diag(diag(R))+diag(X01);
A1=Q*R*Q';

[Q,R]=qr(A);
R=R-diag(diag(R))+diag(X02);
A2=Q*R*Q';
A2 = hess(A2);
A2(n,n) = 1e-16;

[Q,R]=qr(A);
R=R-diag(diag(R))+diag(X03);
A3 = Q*R*Q';

[Q,R]=qr(A);
R=R-diag(diag(R))+diag(X04);
A4 = Q*R*Q';

[Q,R]=qr(A);
R=R-diag(diag(R))+diag(X05);
A5 = Q*R*Q';
A5 = hess(A5);
A5(n,n) = 1e-16;
 
[Q,R]=qr(A);
R=R-diag(diag(R))+diag(X06);
A6= Q*R*Q';

[Q,R]=qr(A);
R=R-diag(diag(R))+diag(X07);
A7= Q*R*Q';


S = {A1, A2, A3, A4, A5, A6, A7};

time = [];
flag = 'matrix';
Delta = [];
X0 = {X01,X02,X03,X04,X05,X06, X07};



for j = 1:7
    j
tic
X = eqr(S{j}, eps);
time = [time toc];
Delta = [Delta norm(X - X0{j}')];
end
