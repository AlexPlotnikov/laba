clc, clear all, close all;

Er = [];
Size = [];
time = [];



for n = 12 : 25
    tic
    G = numgrid( 'L', n);
    A = delsq(G);
    X = ones(size(A,1), 1);
    eps = 10.^(-15)
    B = A * X;
    x0 = zeros(size(A,1), 1);
    [ L ] = IC(A);
    [X1, error, niter, flag,ds,sd] = conjgrad( A, x0, B, size(A, 1).^3 , eps, L);
    t = toc;
    Er = [ Er error];
    Size = [Size size(A,1)];
    time = [time t];
end
figure
plot(Size, Er)
title("График зависимости погрешности от размера матрицы")
xlabel("Размер матрицы")
ylabel("Погрешность")
grid on

figure
plot(Size, time)
title("График зависимости времени выполнения от размера матрицы")
xlabel("Размер матрицы")
ylabel("Время выполнения")
grid on


Er = [];
Iter = [];
Eps = [];

for i = -15 : -1
    G = numgrid( 'L', 20);
    A = delsq(G);
    X = ones(size(A,1), 1);
    eps = 10.^( i )
    B = A * X;
    x0 = zeros(size(A,1), 1);
    [ L ] = IC(A);
    [X1, error, niter, flag,ds,sd] = conjgrad( A, x0, B, size(A, 1).^3 , eps, L);
    Eps = [Eps eps];
    Er = [Er error];
    Iter = [Iter niter];
end

figure
loglog(Eps, Er)
hold on
loglog(Eps, Eps)
title("График зависимости погрешности от заданной точности")
xlabel("Заданная точность")
ylabel("Погрешность")
grid on

figure
semilogx(Eps, Iter)
title("График зависимости количества итераций от заданной точности")
xlabel("Заданная точность")
ylabel("Количество итераций")
grid on

G = numgrid( 'L', 20);
A = delsq(G);
X = ones(size(A,1), 1);
eps = 10.^( -15 )
B = A * X;
x0 = zeros(size(A,1), 1);
[ L ] = IC(A);
[X1, error, niter, flag,M,N] = conjgrad( A, x0, B, size(A, 1).^3 , eps, L);

figure
plot(N, M)
title("График зависимости погрешности от количества итераций")
xlabel("Количество итераций")
ylabel("Погрешность")
grid on

figure
loglog(N, M)
title("График зависимости погрешности от количества итераций в логарифмическом масштабе")
xlabel("Количество итераций")
ylabel("Погрешность")
grid on




