function [ A ] = IC(A)
    n = size(A,1);
    
    for k = 1 : n
        A(k, k) = sqrt(A (k, k));
        
        for i = (k+1) : n
            if A(i, k) ~= 0
                A(i, k) = A(i, k)/A(k, k);
            end
        end
        
        for j = (k+1) : n
            for i = j : n
                if A(i, j) ~= 0 
                    A(i, j) = A(i,j) - A(i, k) * A(j, k);
                end
            end
        end
        
    end
    for i = 1 : n
        for j = i+1 : n
            A(i, j) = 0;
        end
    end
end
