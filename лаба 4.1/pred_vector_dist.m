clc, close all, clear all;
G = numgrid( 'L', 20);
A = delsq(G);
X = ones(size(A,1), 1);
eps = 10.^(-15)
B = A * X;
x0 = zeros(size(A,1), 1);
V = [];
Er = [];
for k = 0 : 3
    V = [V k];
    F = B;
    p = F(1);
    for i = 1 : length(B)
        if F(i) > p
            p = F(i);
            f = i;
        else
            f=1;
        end
    end
    if rand < 0.5
        d = 1;
    else 
        d = -1;
    end
    F(f) = p * (1 + 0.01 * k * d);
    [ L ] = IC(A);
    [X1, error, niter, flag] = conjgrad( A, x0, F, size(A, 1).^3 , eps, L);
    Er = [Er error];
end
plot(V, Er)
grid on
title("Зависимость относительной погрешности от возмущения максимального элемента столбца правой части")
xlabel("Процент возмущения")
ylabel("Погрешность")