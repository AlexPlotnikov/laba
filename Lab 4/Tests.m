clc, clear all, close all;
fun = @(x, y) 2 * x * (x.^2 + y);
fun1 = @(x) exp(x.^2) - x.^2 - 1;
a = 1;
b = 2;
y0 = exp(1) - 2;

[~, XX1, YY1, h1] = Euler(fun, 1, a, b, y0, 1e-10, 2);
[~, XX2, YY2, h2] = Euler(fun, 1, a, b, y0, 1e-10, 6);

x = 1 : 0.005 : 2;
y = fun1(x);

figure 
plot(x, y,'.', XX1, YY1, XX2, YY2)
grid on
title("Графики точного и численных решений")
legend("Точное решение", "Решение, полученное с шагом h = " + h1, "Решение, полученное с шагом h = " + h2)

y = fun1(XX2);

figure
plot(XX2, abs(YY2 - y))
grid on
title("График ошибки на отрезке")
xlabel("Координата")
ylabel("Ошибка")

Eps = zeros(1, 10);
Err = zeros(1, 10);
H = zeros(1, 10);
Iter = zeros(1, 10);

Y0 = fun1(b);

for i = 1 : 10
    
    eps = 10^-i;
    [Y, ~, ~, h, iter] = Euler(fun, 1, a, b, y0, eps, 50);
    
    Eps(i) = eps;
    Err(i) = abs(Y - Y0);
    H(i) = h;
    Iter(i) = iter;
    
end

figure
loglog(Eps, Err, [1e-1, 1e-10], [1e-1, 1e-10])
grid on
title("График зависимости фактической ошибки от заданной точности")
xlabel("Точность")
ylabel("Ошибка")

