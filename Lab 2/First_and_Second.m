clc, clear all, close all;
Er_ch1 = [];
Er_ch2 = [];
Er_v1 = [];
Er_v2 = [];
Otdel = [];
S = rand(10);
[v, d] = eig(S);
M = d(1, 1);
marker = 1;
for i = 1 : 10
    if d(i, i) > M
        M = d(i, i);
        marker = i;
    end
end
AV = v(:, marker);

for i = 0.1 :0.1 : 1.5
    A = M;
    x = [ 0.1 : i : (0.1 + (9 * i)) ];
    [Q,R]=qr(A);
    R = R - diag(diag(R)) + diag(x);
    A = Q * R * Q';
    Otdel = [Otdel i];
    [lambda1, SV1, niter] = Scalar(A, 10^(-15), 1000);
    [lambda2, SV2, kiter] = LRalg(A, 10^(-15), 1000);
    Er_ch1 = [Er_ch1 abs(M - lambda1)];
    Er_ch2 = [Er_ch2 abs(M - lambda2)];
    COS1 = (AV' * SV1) / (norm(AV) * norm(SV1));
    SIN1 = (1 - COS1 ^ 2) .^ 0.5;
    COS2 = (AV' * SV2) / (norm(AV) * norm(SV2));
    SIN2 = (1 - COS2 ^ 2) .^ 0.5;
    Er_v1 = [Er_v1 SIN1];
    Er_v2 = [Er_v2 SIN2];  
end
plot(Otdel, Er_ch1,'o')
hold on
plot(Otdel, Er_ch2)
grid on
title('Зависимость погрешности собственных чисел от отделимости');
xlabel('Отделимость');
ylabel('Погрешность');
legend('Метод скалярных произведений','LR преобразование');
figure
plot(Otdel, Er_v1)
hold on
plot(Otdel, Er_v2)
grid on
title('Зависимость погрешности собственных векторов от отделимости');
xlabel('Отделимость');
ylabel('Погрешность');
legend('Метод скалярных произведений','LR преобразование');


La1 = [];
La2 = [];
Lb1 = [];
Lb2 = [];
N = [];
for n = 10 : 5 : 50
    A = rand(n);
    B = A + A';
    [lambda1, SV1, niter] = Scalar(A, 10^(-15), 1000);
    [lambda2, SV2, kiter] = LRalg(A, 10^(-15), 1000);
    [lambda11, SV11, nniter] = Scalar(B, 10^(-15), 1000);
    [lambda22, SV22, kkiter] = LRalg(B, 10^(-15), 1000);
    La1 = [La1 lambda1];
    La2 = [La2 lambda11];
    Lb1 = [Lb1 lambda2];
    Lb2 = [Lb2 lambda22];
    N = [N n];
end
figure
plot(N, La1)
hold on
plot(N, La2)
hold on
plot(N, Lb1)
hold on
plot(N, Lb2)
grid on
title('Проверка работы методов для симметричных и несимметричных матриц');
xlabel('размер матрицы');
ylabel('Собственные числа');
legend('Метод скалярных произведений для несимм. матриц','Метод скалярных произведений для симм. матриц','LR преобразование для несимм. матриц','LR преобразование для симм. матриц');