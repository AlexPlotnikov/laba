function [lambda, SV, k] = Scalar (A, eps, maxit)
n = size(A, 1);
%X = rand(n, 1);
X  = ones(n, 1);
L = [];

for k = 2 : maxit
    Ch1 = (A ^(k)) * X;
    Ch2 = (A' ^(k)) * X;
    Z1 = (A ^(k-1)) * X;
    Z2 = (A' ^(k-1)) * X;

    Ch = 0;
    Z = 0;
    for i = 1 : n
        Ch = Ch + Ch1( i ) * Ch2( i );
        Z = Z + Z1( i ) * Z2( i );
    end
    L = [ L Ch / Z];
    if length( L ) > 2
        if ( L( end ) - L( end -1 ) ) < eps
            break
        end
    end
end
lambda = (L( end )) ^ (1 / 2) ;
SV = Ch1;
end