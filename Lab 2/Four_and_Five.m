clc, clear all, close all;
Eps = [];
ErCh1 = [];
ErV1 = [];
n = 10;
A = rand(n);
[v, d] = eig(A);
Md = d(1, 1);
marker = 1;
for i =1 : n
    if Md < d(i, i)
        Md = d(i, i);
        marker = i;
    end
end

AV = v(:, marker);
scA = 0;
aa = A * AV;
for i = 1 : n - 1
    scA = aa(i) + aa(i + 1);
end
COSa = (scA) / (norm(A) * norm(AV));
SINa = (1 - COSa ^ 2) .^ 0.5;

for i = -15 : 0
    Eps = [Eps 10 ^ i];
    eps = 10 ^ i;
    [lambda1, SV, k] = Scalar (A, eps, 1000);
    ErCh1 = [ErCh1 abs(Md - lambda1)];
    
    scA1 = 0;
    aa1 = A * SV;
    for i = 1 : n - 1
        scA1 = aa1(i) + aa1(i + 1);
    end
    COSa1 = (scA1) / (norm(A) * norm(SV));
    SINa1 = (1 - COSa1 ^ 2) .^ 0.5;
   
      ErV1 = [ErV1 abs(SINa - SINa1)];
end
loglog(Eps, ErCh1)
grid on
title('Зависимость погрешности собственных чисел от заданной точности');
xlabel('Заданная точность');
ylabel('Погрешность');
figure
loglog(Eps, ErV1)
grid on
title('Зависимость погрешности собственных векторов от заданной точности');
xlabel('Заданная точность');
ylabel('Погрешность');

A = rand(10);
Xch = [1 : 10];
Xpl = [0.1 : 0.1 : 1];
[Q,R]=qr(A);
Rch=R-diag(diag(R))+diag(Xch);
Rpl=R-diag(diag(R))+diag(Xpl);
Ach=Q*Rch*Q';
Apl=Q*Rpl*Q';
Eps = [];
Itch = [];
Itpl = [];
for i = -15 : 0
    Eps = [Eps 10 ^ i];
    eps = 10 ^ i;
    [lambda1, SV, k1] = Scalar (Ach, eps, 1000);
    [lambda2, SbV, k2] = Scalar (Ach, eps, 1000);
    Itch = [Itch k1];
    Itpl = [Itpl k2];
end
figure
semilogx(Eps, Itch, 'o')
hold on
semilogx(Eps, Itpl)
grid on
title('Зависимость количества итераций от заданной точности');
xlabel('Заданная точность');
ylabel('Количество итераций');
legend('Для хорошей отделимости собственных чисел','Для плохой отделимости собственных чисел');
