function [lambda, X, iter] = LRalg(A, eps, maxit)
    n = size(A, 1);
    Start = A;
    for i = 1 : maxit
        iter = i;
        [L, U, P] = lu(A);
        T = [];
        for i = 1 : n
            T = [T A(i, i)];
        end
        M = U * L;
        S = [];
        for i = 1 : n
            S = [S M(i, i)];
        end

        if abs(norm(T) - norm(S)) < eps
            break
        end
        A = M;
    end

    lambda = max(S);
    b = [];
    Sub = zeros(n - 1);
    for i = 1 : n - 1
        b = [b Start(i, n)];
        for j = 1 : n - 1
            Sub(i,j) = Start(i, j);
        end
    end
    X = Sub\b';
    X = X';
    X = [X 1];
    X = X';
    
end