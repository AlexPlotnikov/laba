clc, clear all, close all;
D = [];
ER_ch = [];
ER_v = [];
ER_ch1 = [];
ER_v1 = [];
n = 10;
A = rand(n);
[v, d] = eig(A);
M = d(1, 1);
marker = 1;
for i = 1 : n
    if d(i, i) > M
        M = d(i, i);
        marker = i;
    end
end
AV = v(:, marker);

for i = 1 : 5
    S = A;
    D = [D i];
    for k = 1 : n
        if rand < 0.5
            dist = 1;
        else 
            dist = -1;
        end
        S(1, k) = S(1, k) * (1 + 0.01 * i * dist);
    end
    [lambda, SV, niter] = Scalar(S, 10^(-15), 1000);
    [lamb, SbV, kiter] = LRalg(S, 10^(-15), 1000);
    ER_ch = [ER_ch (abs(M - lambda))];
    ER_ch1 = [ER_ch1 (abs(M - lamb))];

    
    COS1 = (AV' * SV) / (norm(AV) * norm(SV));
    SIN1 = (1 - COS1 ^ 2) .^ 0.5;
    COS2 = (AV' * SbV) / (norm(AV) * norm(SbV));
    SIN2 = (1 - COS2 ^ 2) .^ 0.5;
    ER_v = [ER_v (abs(SIN1))];
    ER_v1 = [ER_v1 (abs(SIN2))];
end
plot(D, ER_ch)
hold on
plot(D, ER_ch1)
grid on
title('Зависимость погрешности собственных чисел от возмущений в матрице');
xlabel('Процент возмущений в строке');
ylabel('Погрешность');
legend('Метод скалярных произведений','LR преобразование');
figure
plot(D, ER_v)
hold on
plot(D, ER_v1)
title('Зависимость погрешности собственных векторов от возмущений в матрице');
xlabel('Процент возмущений в строке');
ylabel('Погрешность');
grid on

