clc
clear all
close all
eps = 1e-10;
f = @(x)x.^4 - 6.2*x.^3+3.5*x.^2-7*x-2.1;
Q = integral(f, -1, 1);
[q,i, step, pV] = Simp(f,-1,1,eps);
Delta = abs(pV-Q);

figure 
Ls = log2(step);
LD = log2(Delta);
plot(Ls, LD)
grid on
title('График зависимости фактической погрешности от шага сетки')
xlabel('Шаг')
ylabel('Погрешность')

p = polyfit(Ls,LD,1);
n = p(1)
C = 2^(p(2))

