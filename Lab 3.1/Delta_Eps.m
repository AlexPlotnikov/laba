clc
clear all
close all
f = @(x)x.^4 - 6.2*x.^3 + 3.5*x.^2 - 7*x - 2.1;
Q = integral(f,0,3);
E = [];
Delta = [];
N = [];
for i = 1:13
  eps = 10^(-i);
  E = [E eps];
[q,i] = Simp(f,0,3,eps);
Delta = [Delta abs(q-Q)];
N = [N i];
end

figure
y = @(x) x;
x = 1e-13:0.1:1;
loglog(x,y(x))
hold on
loglog(E,Delta)
grid on
title('График зависимости погрешности от точности')
xlabel('Заданная точность')
ylabel('Погрешность')
legend('y = x')

figure
semilogx(E, N)
grid on
title('График зависимости кол-ва итераций от точности')
xlabel('Заданная точность')
ylabel('Количество итераций')
