function [y, XX, YY] = Euler_Imp(fun, a, b, Y0, h)
    
    n = floor((b - a) / h);
    
    YY = zeros(1, n + 1);
    XX = zeros(1, n + 1);
    
    x0 = a;
    y0 = Y0;
    
    XX(1) = x0;
    YY(:, 1) = y0;
    
    for j = 1 : n
        
        fun1 = @(y)y - y0 - h * fun(x0, y);
        y = Newton(fun1, a, 1000, 1e-15);
        
        YY(:, j + 1) = y;
        
        x0 = x0 + h;
        XX(j + 1) = x0;
        
        y0 = y;
        
    end
    
end