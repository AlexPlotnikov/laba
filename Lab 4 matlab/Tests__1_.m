% Эксперименты на устойчивость явного и неявного методов Эйлера
fun = @(x, y)5 * (y - x.^2);
fun1 = @(x)x.^2 + 0.4 * x + 0.08;
y0 = 0.08;

a = 0;

H = 0.1 : -0.01 : 0.01;

k = 1;
figure
for i = 0.08 : -0.01 : 0.01
    
    b = 0.5 + 0.1 * (k - 1);
    
    [~, XX, YY] = Euler_Exp(fun, 1, a, b, y0, i);
    
    k = k + 1;
    
    txt = ['h = ', num2str(i), ', длина промежутка = ', num2str(b - a)];
    plot(XX, YY, 'DisplayName', txt);
    hold on
    
end
legend show
grid on 
title('Графики решений, полученных явным методом Эйлера для разного шага и разной длины промежутков')

a = 0.08;
b = 0.5;

figure
for i = 0.1 : -0.01 : 0.01
    
    [~, XX, YY] = Euler_Exp(fun, 1, a, b, y0, i);
    
    txt = ['h = ', num2str(i)];
    plot(XX, YY, 'DisplayName', txt);
    hold on
    
end
legend show
grid on 
title('Графики решений, полученных явным методом Эйлера для разного шага на промежутке [0.08; 0.5]')

a = 0;

for j = 1 : 3
    
    b = 0.5 + (j - 1) * 0.5;
    k = 1;
    
    Err = zeros(1, length(H));
    
    c = b - a;
    
    for i = 0.1 : -0.01 : 0.01
        
        y = Euler_Exp(fun, 1, a, b, y0, i);
        Err(k) =  abs(fun1(b) - y);
        
        k = k + 1;
        
    end
    
    figure
    plot(H, Err);
    grid on 
    title('График зависимости ошибки от величины шага и длины промежутка для явного метода Эйлера')
    legend("Длина промежутка = " + c)
    xlabel("Шаг")
    ylabel("Ошибка")
    
end

fun = @(x, y)-100 * y + 10;
fun1 = @(x)0.1 + 0.9 * exp(-100 * x);
y0 = 1;

a = 0;
b = 10;

H = 0.1 : -0.01 : 0.01;

figure
for i = 0.08 : -0.01 : 0.01
    
    [~, XX, YY] = Euler_Exp(fun, 1, a, b, y0, i);
    [~, XX1, YY1] = Euler_Imp(fun, a, b, y0, i);
    
    txt = ['h = ', num2str(i), ', явный метод Эйлера'];
    plot(XX, YY, 'DisplayName', txt);
    txt1 = ['h = ', num2str(i), ', неявный метод Эйлера'];
    plot(XX1, YY1, 'DisplayName', txt1);
    hold on
    
end
legend show
grid on 
title('Графики решений, полученных явным и неявным методами Эйлера для разного шага [0; 10]')


a = 0.08;
b = 1;

figure
for i = 0.08 : -0.01 : 0.01
    
    [~, XX, YY] = Euler_Exp(fun, 1, a, b, y0, i);
    [~, XX1, YY1] = Euler_Imp(fun, a, b, y0, i);
    
    txt = ['h = ', num2str(i), ', явный метод Эйлера'];
    plot(XX, YY, 'DisplayName', txt);
    txt1 = ['h = ', num2str(i), ', неявный метод Эйлера'];
    plot(XX1, YY1, 'DisplayName', txt1);
    hold on
    
end
legend show
grid on 
title('Графики решений, полученных явным и неявным методами Эйлера для разного шага на промежутке [0.08; 1]')

Err1 =  [1, length(H)];
Err2 =  [1, length(H)];

a = 0;
k = 1;
for i = 0.1 : -0.01 : 0.01
    
    y = Euler_Exp(fun, 1, a, b, y0, i);
    y1 = Euler_Imp(fun, a, b, y0, i);
    Err1(k) =  abs(fun1(b) - y);
    Err2(k) =  abs(fun1(b) - y1);
    
    k = k + 1;
    
end

figure
semilogy(H, Err1, H, Err2);
grid on
title('График зависимости ошибки от величины шага для явного и неявного методов Эйлера')
xlabel("Шаг")
ylabel("Ошибка")
legend('Явный метод Эйлера', 'Неявный метод Эйлера')
