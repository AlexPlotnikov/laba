% Эксперименты для функций MatLab
fun = @(x, y) 2 * x * (x.^2 + y);
fun1 = @(x) exp(x.^2) - x.^2 - 1;
a = 1;
b = 2;
y0 = exp(1) - 2;

Eps = zeros(1, 10);

Err1 = zeros(1, 10);
Err2 = zeros(1, 10);
Err3 = zeros(1, 10);
Err4 = zeros(1, 10);
Err5 = zeros(1, 10);
Err6 = zeros(1, 10);
Err7 = zeros(1, 10);

Iter1 = zeros(1, 10);
Iter2 = zeros(1, 10);
Iter3 = zeros(1, 10);
Iter4 = zeros(1, 10);
Iter5 = zeros(1, 10);
Iter6 = zeros(1, 10);
Iter7 = zeros(1, 10);

for i = 1 : 10
    
    eps = 10^-i;
    opts = odeset('RelTol',eps,'AbsTol',eps);
    
    [t1, y1] = ode45(fun, [a b], y0, opts);
    [t2, y2] = ode23(fun, [a b], y0, opts);
    [t3, y3] = ode113(fun, [a b], y0, opts);
    
    [t4, y4] = ode15s(fun, [a b], y0, opts);
    [t5, y5] = ode23s(fun, [a b], y0, opts);
    [t6, y6] = ode23t(fun, [a b], y0, opts);
    [t7, y7] = ode23tb(fun, [a b], y0, opts);
    
    Err1(i) = abs(y1(end) - fun1(b));
    Err2(i) = abs(y2(end) - fun1(b));
    Err3(i) = abs(y3(end) - fun1(b));
    Err4(i) = abs(y4(end) - fun1(b));
    Err5(i) = abs(y5(end) - fun1(b));
    Err6(i) = abs(y6(end) - fun1(b));
    Err7(i) = abs(y7(end) - fun1(b));

    Iter1(i) = length(t1);
    Iter2(i) = length(t2);
    Iter3(i) = length(t3);
    Iter4(i) = length(t4);
    Iter5(i) = length(t5);
    Iter6(i) = length(t6);
    Iter7(i) = length(t7);
    
    Eps(i) = eps;
    
end

figure
loglog(Eps, Err1, Eps, Err2, Eps, Err3, Eps, Err4, Eps, Err5, Eps, Err6, Eps, Err7)
grid on 
title("График зависимости фактической ошибки от заданной точности")
xlabel("Точность")
ylabel("Ошибка")
legend("ode45", "ode23", "ode113", "ode15s", "ode23s", "ode23t", "ode23tb")

figure
loglog(Eps, Iter1, Eps, Iter2, Eps, Iter3, Eps, Iter4, Eps, Iter5, Eps, Iter6, Eps, Iter7)
grid on 
title("График зависимости затраченного на нахождение решения числа итераций от заданной точности")
xlabel("Точность")
ylabel("Число итераций")
legend("ode45", "ode23", "ode113", "ode15s", "ode23s", "ode23t", "ode23tb")

H1 = zeros(1, length(t1));
H2 = zeros(1, length(t2));
H3 = zeros(1, length(t3));
H4 = zeros(1, length(t4));
H5 = zeros(1, length(t5));
H6 = zeros(1, length(t6));
H7 = zeros(1, length(t7));

for i = 1 : length(t1) - 1
    H1(i) = t1(i + 1) - t1(i);
end

for i = 1 : length(t2) - 1
    H2(i) = t2(i + 1) - t2(i);
end

for i = 1 : length(t3) - 1
    H3(i) = t3(i + 1) - t3(i);
end

for i = 1 : length(t4) - 1
    H4(i) = t4(i + 1) - t4(i);
end

for i = 1 : length(t5) - 1
    H5(i) = t5(i + 1) - t5(i);
end

for i = 1 : length(t6) - 1
    H6(i) = t6(i + 1) - t6(i);
end

for i = 1 : length(t7) - 1
    H7(i) = t7(i + 1) - t7(i);
end

figure
semilogy(t1, H1, t2, H2, t3, H3, t4, H4, t5, H5, t6, H6, t7, H7)
grid on 
title("График изменения шага на отрезке")
xlabel("Координата")
ylabel("Длина отрезка разбиения")
legend("ode45", "ode23", "ode113", "ode15s", "ode23s", "ode23t", "ode23tb")