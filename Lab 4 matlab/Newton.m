function [a] = Newton(fun, a, Nmax, eps)

    for i = 1 : Nmax
        
        f = fun(a);
        der = (fun(a + 1e-4) - fun(a - 1e-4)) / (2 * 1e-4);
        
        a = a - f/der;
        
        if abs(f) <= eps
            break
        end
        
    end
    
end