function [x] = Rotation(A, b)

    N = length(A);
    
    x = zeros(N, 1);
    
    for i = 1 : N - 1
        for j = i + 1 : N
            
            c = A(i, i)/sqrt(A(i, i)^2 + A(j, i)^2);
            s = A(j, i)/sqrt(A(i, i)^2 + A(j, i)^2);
            
            A1 = c * A(i, :) + s * A(j, :);
            A2 = -s * A(i, :) + c * A(j, :);
            A(i, :) = A1;
            A(j, :) = A2;

            b1 = c * b(i) + s * b(j);
            b2 = -s * b(i) + c * b(j);
            b(i) = b1;
            b(j) = b2;
            
        end
    end
    
    for i = N : -1 : 1
        for j = 1 : N - i
            
            x(i) = x(i) - A(i, N - j + 1)*x(N - j + 1);
            
        end
        
        x(i) = (x(i) + b(i))/A(i,i);
        
    end
    
end