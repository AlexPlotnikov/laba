% Эксперименты для жестких систем ОДУ
S = linspace(1e2, 1e6, 20);
y0 = [1; 0; 1; 0; 1; 0; 1; 0; 1; 0];
a = 0;
b = 10;

H = 0.1 : -0.01 : 0.01;
ERR1 = zeros(length(H), 20);
ERR2 = zeros(length(H), 20);

for i = 1 : 20
    
    L = linspace(-1, -S(i), 10);
    L = diag(L);
    V = rand(10);
    A = V * L * inv(V);
    
    fun = @(x, y)A * y;
    
    opts = odeset('RelTol', 1e-10,'AbsTol', 1e-15);
    [~, Y0] = ode15s(fun, [a b], y0, opts);
    
    Err1 = zeros(length(H), 1);
    Err2 = zeros(length(H), 1);
    
    k = 1;
    
    for j = 0.1 : -0.01 : 0.01
        
        y1 = Euler_Exp(fun, 10, a, b, y0, j);
        y2 = Euler_ImpVec(A, 10, a, b, y0, j);
        
        Err1(k) = abs(max(Y0(end, :) - y1'));
        Err2(k) = abs(max(Y0(end, :) - y2'));
        
        k = k + 1;
        
    end
    
    ERR1(:, i) = Err1;
    ERR2(:, i) = Err2;
    
end

figure
for i = 1 : 20
    txt = ['S = ', num2str(S(i))];
    semilogy(H, ERR1(:, i), 'DisplayName', txt, 'Color', [rand, rand, rand])
    hold on
end
grid on
title('График зависимости фактической ошибки от шага метода для разных чисел жесткости системы, явный метод Эйлера')
legend show
hold off

figure
for i = 1 : 20
    txt = ['S = ', num2str(S(i))];
    semilogy(H, ERR2(:, i), 'DisplayName', txt, 'Color', [rand, rand, rand])
    hold on
end
grid on
title('График зависимости фактической ошибки от шага метода для разных чисел жесткости системы, неявный метод Эйлера')
legend show
hold off