function [y, XX, YY] = Euler_ImpVec(A, N, a, b, Y0, h)
    
    n = floor((b - a) / h);
    
    YY = zeros(N, n + 1);
    XX = zeros(1, n + 1);
    
    x0 = a;
    y0 = Y0;
    
    XX(1) = x0;
    YY(:, 1) = y0;
    
    for j = 1 : n
        
        y = Rotation(eye(N) - h * A, y0);
        
        YY(:, j + 1) = y;
        
        x0 = x0 + h;
        XX(j + 1) = x0;
        
        y0 = y;
        
    end
    
end