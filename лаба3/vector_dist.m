clc, close all, clear all;
s = 10;
X = ones(s, 1); 
V = [];
Er = [];
A = rand(s);
A = A + A' + 10 * eye(s);
B = A * X;
for k = 0 : 3
    V = [V k];
    F = B;
    p = F(1);
    for i = 1 : length(B)
        if F(i) > p
            p = F(i);
            f = i;
        else
            f=1;
        end
    end
    if rand < 0.5
        d = 1;
    else 
        d = -1;
    end
    F(f) = p * (1 + 0.01 * k * d);
    [L, D] = LDLT(A);
    Y = L \ F;
    X0 = (D * L') \ Y;
    c = 100 * norm( X0 - X ) / norm(X);
    Er = [Er c];
end
plot(V, Er)
grid on
title("Зависимость относительной погрешности от возмущения максимального элемента столбца правой части")
xlabel("Процент возмущения")
ylabel("Относительная погрешность (%)")