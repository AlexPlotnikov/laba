clc
clear all
close all

f = @(x) (x.^4 -6.2.*x.^3 + 3.5.*x.^2 - 7.*x - 2.1).*cos(2*x);
a = 0;
b = 2;
N = [];
E = [];
for i = 1:15
eps = 10^(-i);
E = [E eps];
[I,R,H,iter] = Lobatto(f,a,b,1000,eps);
N = [N iter];
end

figure
plot(log2(E), log2(N))
grid on
xlabel('\epsilon');
ylabel('N');
title('Зависимость кол-ва итераций от заданной точности');
