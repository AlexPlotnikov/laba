clc
clear all
close all

f = @(x) (x.^4 -6.2.*x.^3 + 3.5.*x.^2 - 7.*x - 2.1).*cos(2*x);
a = 0;
b = 8;
eps = 1e-14;
syms x
Q = int(f,x,a,b);
Delta = [];


[I,R,H,i,E, Is] = Lobatto(f,a,b,1000,eps);

Hl = [];
for i = 1:length(H)
if log2(H(i))>=0
Hl = [Hl log2(H(i))];
Delta = [Delta abs(Q-Is(i))];
end
end
Dl = double(log2(Delta));
p = polyfit(Hl,Dl,1);
n = p(1)
C = 2^(p(2))

figure
plot(Hl,Dl)
grid on
xlabel('\epsilon');
ylabel('\delta');
title({'Зависимость фактической ошибки от', 'длины отрезка разбиения'});
