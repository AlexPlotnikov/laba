clc
clear all
close all
 f = @(x)(x.^4 - 7.2.*x.^3 + 9.5.*x.^2 - 7.*x - 2.5).*cos(2*x);
a = 0;
b = 2;
n =4;
syms x
Q = int(f,x,a,b);
E = 10.^(-1:-1:-n);
Delta = zeros(3,n);
N = zeros(3,n);
for j = 1:3
    f = @(x)(x.^4 - 6.2.*x.^3 + 3.5.*x.^2 - 7.*x - z(j)*2.1).*cos(2*x);
for i = 1:n
  eps = 10^(-i);
[I,R,H,k] = Lobatto(f,a,b,1000,eps);
Delta(j,i) =  abs(I-Q);
N(j,i) =  k;
end
end


figure
y = @(x) x;
x = 1e-4:0.01:0.1;
loglog(x,y(x))
grid on
hold on
loglog(E,Delta(1,:))
hold on
loglog(E,Delta(2,:))
hold on
loglog(E,Delta(3,:))
legend('y = x', '1%','2%','3%')
title({'График зависимости погрешности от', "точности с возмущением"})
xlabel('\epsilon')
ylabel('\delta')


figure
semilogx(E,N(1,:))
grid on
hold on
semilogx(E,N(2,:))
hold on
semilogx(E,N(3,:))
legend( '1%','2%','3%')
title({'График зависимости кол-ва итераций', "от точности с возмущением"})
xlabel('\epsilon')
ylabel('N')


function [dist]= z(alfa)
if alfa  == 0
    dist = 1;
else  
dist = 1 + sign(randi([-1,1]))*(rand()/100+(alfa-1)/100);
if dist == 1
    [dist] = z(alfa);
end
end
end