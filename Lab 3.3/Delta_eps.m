clc
clear all
close all

f = @(x) (x.^4 -6.2.*x.^3 + 3.5.*x.^2 - 7.*x - 2.1).*cos(2*x);
a = 0;
b = 2;

syms x
Q = int(f,x,a,b);
E = [];
Delta1 = [];
Delta2 =[];



for i = 1:13
eps = 10^(-i);
E = [E eps];
[I,R] = Lobatto(f,a,b,1000,eps);
Delta1 = [Delta1 abs(Q-I)];
Delta2 = [Delta2 abs(Q-R)];
end

figure
plot(log2(E),log2(Delta2))
grid on
hold on
plot(log2(E),log2(Delta1))
hold on
y = @(x) x;
x = 1e-15:0.1:1;
plot(log2(x),log2(y(x)))
xlabel('\epsilon');
ylabel('\delta');
title('Зависимость фактической ошибки от заданной точности');
legend('Ошибка', 'Ошибка для уточненного знач.','y = x');
