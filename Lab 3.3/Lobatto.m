function [I, R, H, k, E, Is] = Lobatto(f, a, b, N, eps)

h = b - a;

n = 1;

n1 = 4;
n2 = 5;

[t1, A1] = Calc_node(n1);
[t2, A2] = Calc_node(n2);

K1 = 2/((n1 + 1)*(n1 + 2));
K2 = 2/((n2 + 1)*(n2 + 2));

H = [];
Is = [];

for k = 1:N
    
I = 0;
R = 0;
E = 0;

s = a;

    for i = 1:n
        
        m = a + (i - 1)*h + h/2;
        e = m + h/2;
        
        [I11, I21] = Lob_hlp(f, s, m, t1, t2, A1, A2, K1, K2);
        [I12, I22] = Lob_hlp(f, m, e, t1, t2, A1, A2, K1, K2);
        
        delta1 = (I21 - I11)/1023;
        delta2 = (I22 - I12)/1023;
        
        E = E + abs(delta1) + abs(delta2);
        I = I + I21 + I22;
        R = R + I21 + I22 + delta1 + delta2;
     
        s = e;
        
    end
    
H = [H h];
Is = [Is I];

if E < eps
    return
end

h = h/2;
n = n*2;

end
