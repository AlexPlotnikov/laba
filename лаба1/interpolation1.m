clc, clear all, close all;
format long

%% Чтобы получить конкретный график или значение снимите комментарий слева от 
%% названия функции. Справа написано, что она делает
fun = @(x) x.^4 + 3*x.^3 - 9*x -9;
%oki1(0.2,fun)% График функции в окрестности корня(корней)  

%oki3(fun,1,2,2.44,10.^(-14),2) % график зависимости  погрешности от кол-ва итераций
%oki4(fun,1,2,2.44,10.^(-14),2)% График зависимости погрешности от заданной точности
%oki5(fun,1,2,2.44,10.^(-14))% График зависимости использованного числа вычислений функции от заданной точности
%oki6(fun,1,2,2.44,10.^(-14),2)% График зависимости относительной погрешности решения от возмущения исходных данных
%oki7(fun,2)% Значение корня, полученное с помощью fzero и roots



function []= oki1(a, fun)

x=[];
y=[];
    for i=-4:a:2.44
        x=[x i];
    end
        for j= 1:1:length(x)
            y(j)=fun(x(j));
        end
    
 plot(x,y)
grid minor
end


function[]= oki3(fun,x1,x2,x3,eps,x0)
xx=fzero(fun,x0);

    [x3,k,Kol,error]=oki2(fun,x1,x2,x3,eps,100);
for h =1:length(error)
    error(h)=abs(error(h)-xx);
end
plot(Kol,error)
grid minor
figure
loglog(Kol,error)
grid minor
end

function [] = oki4(fun,x11,x22,x33,eps, x0)
xx=fzero(fun,x0);
ERROR=[];
Eps=[];
error=[];
    for r = 1:1:15
  
    [x3,k,A,B]= oki2(fun,x11,x22,x33,eps, 100);
    error=[error x3];
   
    Eps=[Eps eps];
    eps = eps*10;
    
    end %%for r

for I=1:15
G= abs(xx-error(I));
ERROR=[ERROR G];
end

loglog(Eps,ERROR)
hold on
loglog(Eps,Eps)
grid minor
end

function [] = oki5(fun,x11,x22,x33,eps)
Kol=[];
    for r = 1:1:15
    
    
    [x3,k,A,B]= oki2(fun,x11,x22,x33,eps, 100);
    Kol=[Kol k];
   
   
    eps = eps*10;
    
    end %%for r


EPS=[ -14 -13 -12 -11 -10 -9 -8 -7 -6 -5 -4 -3 -2 -1 0];
plot(EPS,Kol)
grid minor
end

function [] = oki6(f,x11,x22,x33,eps,x0)

xx=fzero(f,x0);
Kol=[0, 1, 2, 3, 4, 5];
R=[];
d=0;

for j= 0:1:5
    
        
        if (rand<0.5)
            d = -1;
        else
            d=1;
        end
 
    fun = @(x) x.^4 + 3*x.^3 - 9*x -9*(1+d*0.01*j*d+(rand*j*d*0.001));
    
      [x3,k,A,B]= oki2(fun,x11,x22,x33,eps, 100);
    R(j+1) = x3;
end
Rf=[];
for i=1:6
Rf(i)= abs(xx-R(i))*100/xx;
end
plot(Kol, Rf)
grid minor
end

function[]=oki7(fun,x0)

fzero(fun,x0)
roots([1 3 0 -9 -9])
end