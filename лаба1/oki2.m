function [x3,k,Kol,error] = oki2(fun,x1,x2,x3,eps,n)
Kol=[];
error=[];
F1= fun(x1);
F2= fun(x2);
F3= fun(x3);
    for k =0:n
        time=x3;
        x3=((F2*F3*x1)/((F1-F2)*(F1-F3)))+((F1*F3*x2)/((F2-F1)*(F2-F3)))+((F1*F2*x3)/((F3-F1)*(F3-F2))) ;
        x1=x2;
        x2=time;
        F1=F2;
        F2=F3;
        F3=fun(x3);
        Kol=[Kol k];
        error=[error x3];
        if abs(F3)< eps
            break
        end
    end
end