clc, clear all, close all;
format long
f = @(x) x.^4 - 6.2 * x.^3 + 3.5 * x.^2 -7 * x - 2.1;
a = 1;
b = 3;
exact = integral(f, a, b);
% [I,k] = Ad_Simp (f, a, b, 10^(-14))


Eps = [];
Er = [];
count = 0;
for i = -13 : -1
    eps = 10^i;
    Eps = [Eps eps];
    [I,count, F] = Ad_Simp (f, a, b, eps,count);
    Er = [Er abs(exact - I)];
end
plot(log2(Eps), log2(Er))
hold on
plot(log2(Eps), log2(Eps))
grid on
xlabel('Заданная точность')
ylabel('Ошибка')
title('Зависимость ошибки от точности')

Iter = [];
count = 0;
for i = -13 : -1
    eps = 10^i;
    [I,count, F] = Ad_Simp (f, a, b, eps,count);
    Iter = [count Iter];
end
figure
plot(log2(Eps),log2(Iter))
grid on
xlabel('Заданная точность')
ylabel('Количество итераций')
title('Зависимость количества итераций от точности')

L = [];
X = [];
count = 0;
[I,count, F] = Ad_Simp (f, a, b, 10^(-13),count);
for i = a : 0.1 : b - 0.1
    X = [X i];
    %count = 0;
end
for i = 1 : length(X)
    for j = 1 : length(F)
        if X(i) < F(j)
            L = [L (F(j) - F(j - 1))];
        end
    end
end
figure
plot(X, L)
xlabel('Координата')
ylabel('Длина шага')
title('Зависимость дллины шага от координаты')





