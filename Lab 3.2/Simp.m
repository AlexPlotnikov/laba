function [V, iter, step, pV] = Simp(f, a, b, eps, m)
n = 8 * m;
step = [];
pV = [];
    for j = 1 : 1000000
        h = (b - a) / n;
        step = [step h];
        xi = a : h : b;
        y1 = 0;
        y2 = 0;
        for i = 1 : n / 2
            y1 = y1 + f(xi(2 * i));
            if i < n / 2
            y2 = y2 + f(xi(2 * i + 1));
            end
        end
        I1 = h / 3 * (f(a) + f(b) + 4 * y1 + 2 * y2);
        pV = [pV I1];
        if j == 1
            I = 0;
        end
        if abs(I - I1) < 15 * eps
            V = I1;
            iter = j;
            break
        end
        n = 2 * n;
        I = I1;
    end
end