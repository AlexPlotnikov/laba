clc
clear all 
close all
format long
a = 0;
b = 6;
b2 = pi/2;
f1 = @(x) x.^4 -7.2.*x.^3 + 9.5.*x.^2 - 7.*x - 2.5;
fm = @(x) sign(x - (b-a)/2).*x.^4 -7.2.*x.^3 + 9.5.*x.^2 - 7.*x - 2.5;
fs = @(x) (x.^4 -7.2.*x.^3 + 9.5.*x.^2 - 7.*x - 2.5).*sin(x.^2);
Q = -234.6;
Q1 = -331.8;
Q2 = -6.000598662872363;
E = [];
Delta = [];
Delta1 = [];
Delta2 = [];
Delta3 = [];

for p = 2:10
    eps = 10^(-p);
    E = [E eps];
    q = integral(f1,a,b,'Reltol', 0, "AbsTol",eps);
    q1 = integral(fm,a,b,'Reltol',0, "AbsTol",eps);
     q2 = integral(fs,a,b2,'Reltol',0, "AbsTol",eps);
     q3 = quadgk(fs,a,b2,"RelTol",0,"AbsTol",eps);
    Delta = [Delta abs(Q-q)];
     Delta1 = [Delta1 abs(Q1-q1)];
     Delta2 = [Delta2 abs(Q2-q2)];
     Delta3 = [Delta3 abs(Q2-q3)];
end
figure 
loglog(E, Delta)
hold on
loglog(E, Delta1)
grid on
xlabel('\epsilon')
ylabel('\delta')
title('График зависимости погрешности от точности')
legend('P(x)','P(x) + sign')



figure
loglog(E,Delta2)
hold on
loglog(E, Delta3)
grid on
xlabel('\epsilon')
ylabel('\delta')
title('График зависимости погрешности от точности')
legend('P1(x), integral','P1(x), quadgk')

