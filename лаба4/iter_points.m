clc, clear , close all;
A = rand(10);
A = A + A' + 5 * eye(10);
X = ones(10, 1);
B = A * X;

itslau1(A, B, X, 10^(-15))%График зависимости погрешности от заданной точности
figure
itslau2(A, B, 10^(-15))%График зависимости количества итераций от заданной точности
figure
itslau3(A, B)%Графики зависимости погрешности от количества итераций
function [] = itslau1(A, B, X, eps)
    EPS = [];
    error = [];
    for i = 0 : 15
        [X0,n,sa,as]=zei(A, B,eps,1000);
        error = [error norm(X0 - X)];
        EPS = [EPS eps];
        eps = eps * 10;
    end
    loglog(EPS, error)
    grid on
    title('График зависимости погрешности от заданной точности')
    ylabel('погрешность')
    xlabel('заданная точность')
end
function [] = itslau2(A, B, eps)
    EPS = [];
    Kol = [];
    for i = 0 : 15
        [X0,n,sa,as]=zei(A, B,eps,1000);
        Kol = [Kol n];
        EPS = [EPS eps];
        eps = eps * 10;
    end
    semilogx(EPS, Kol)
    grid on
    title('График зависимости количества итераций от заданной точности')
    ylabel('количество итераций')
    xlabel('заданная точность')
end
function [] = itslau3(A, B)
    [X0,n,Kol,Roots]=zei(A, B,10^(-15),1000);
    plot(Kol,Roots)
    grid on
    title('График зависимости погрешности от количества итераций')
    xlabel('количество итераций')
    ylabel('погрешность')
    figure
    semilogy(Kol,Roots)
    grid on
    title('График зависимости погрешности от количества итераций в логарифмическом масштабе')
    xlabel('количество итераций')
    ylabel('погрешность')
end