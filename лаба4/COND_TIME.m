clc, close all, clear all

X = ones(10, 1);
%cond = 1
A = rand(10);
A = A + A' + 5 * eye(10);
[u, d, v] = svd(A);
d = eye(10);
d(1,1) = 1; %задаем число обусловленности
M = u*d*v'; 
c1 = cond(M);
B = M*X;
tic
[X0,n]=zei(M, B, 10^(-15),1000);
t1 = toc;
h1 = norm(X0 - X);

%cond = 10
A = rand(10);
A = A + A' + 5 * eye(10);
[u, d, v] = svd(A);
d = eye(10);
d(1,1) = 10;
M = u*d*v';
c2 = cond(M);
B = M*X;
tic
[X0,n]=zei(M, B, 10^(-15),1000);
t2 = toc;
h2 = norm(X0 - X);

%cond = 100
A = rand(10);
A = A + A' + 5 * eye(10);
[u, d, v] = svd(A);
d = eye(10);
d(1,1) = 100;
M = u*d*v';
c3 = cond(M);
B = M*X;
tic
[X0,n]=zei(M, B, 10^(-15),1000);
t3 = toc;
h3 = norm(X0 - X);


%cond = 1000
A = rand(10);
A = A + A' + 5 * eye(10);
[u, d, v] = svd(A);
d = eye(10);
d(1,1) = 1000;
M = u*d*v';
c4 = cond(M);
B = M*X;
tic
[X0,n]=zei(M, B, 10^(-15),1000);
t4 = toc;
h4 = norm(X0 - X);

%cond = 100000
A = rand(10);
A = A + A' + 5 * eye(10);
[u, d, v] = svd(A);
d = eye(10);
d(1,1) = 10000;
M = u*d*v';
c5 = cond(M);
B = M*X;
tic
[X0,n]=zei(M, B, 10^(-15),1000);
t5 = toc;
h5 = norm(X0 - X);

%cond = 100000
A = rand(10);
A = A + A' + 5 * eye(10);
[u, d, v] = svd(A);
d = eye(10);
d(1,1) = 100000;
M = u*d*v';
c6 = cond(M);
B = M*X;
tic
[X0,n]=zei(M, B, 10^(-15),1000);
t6 = toc;
h6 = norm(X0 - X);

h = [h1 h2 h3 h4 h5 h6];
c = [c1 c2 c3 c4 c5 c6];
t = [t1 t2 t3 t4 t5 t6];
figure
loglog(c, h)
title("График зависимости ошибки от числа обусловленности")
xlabel("Число обусловленности")
ylabel("Прологарифмированная норма отклонения")
grid on

figure
semilogx(c, t)
title("График зависимости времени от числа обусловленности")
xlabel("Число обусловленности")
ylabel("Прологарифмированное время")
grid on