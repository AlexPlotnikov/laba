function [x1,n,Kol,Roots]= zei (A, B , eps, max)
Kol = [];
n = size( A,1);
Roots = [];
L = zeros(n);
D = zeros(n);
R = zeros(n);
    for i = 1:n
        D(i,i) = A(i,i);
    end
    for i = 2:n
        for j = 1:i-1
            L(i,j) = A(i,j);
            R(j,i) = A(j,i);
        end
    end
 
    x= zeros(n,1);
    for i = 0:max
        n = i+1;
        Kol = [Kol i];
        x1 = -1 * inv(L + D) * R * x + inv(L + D) * B;
        XX = x1 - x;
        P = XX(1);
        for k = 2:length(XX)
            if P < XX(k)
                P = XX(k);
            end
        end
        Roots = [Roots P];
        if P < eps
            break
        end
        x = x1;
    end
end