clc,clear all, close all;
f=@(x) 3.*sign(x).*x.^4 - 8.*x.^3 - 18.*x.^2 +6;

%inter1L(f,5) %(Графики: функции, 3-х интерполяционных полиномов для
             %нее,ошибка)Равномерная сетка
%inter1CHE(f,5) %Графики: функции, 3-х интерполяционных полиномов для
             %нее,ошибка)Чебышевская сетка
%inter2L(f)% (График ошибки от количества узлов)Равномерная сетка
%inter2CHE(f)% (График ошибки от количества узлов)Чебышевская сетка
%inter3L(f,-1,3)%(График oшибки от количества узлов в 2 точках)Равномерная сетка
%inter3CHE(f,-1,3)%(График oшибки от количества узлов в 2 точках)Чебышевская сетка
%inter4L(f,10)%(График относительной погрешности от процента возмущений исходных данных)
            %Равномерная сетка
%inter4CHE(f,10)%(График относительной погрешности от процента возмущений исходных данных)
            %Чебышевская сетка
function [] = inter1L(f,a)
    xx=[];
    yy=[];

    for i=-2:6/(a-1):4
        xx=[xx i];
    end
    for i=1:length(xx)
        yy=[yy f(xx(i))];
    end
    x=[];
    y=[];
    for i=-2:0.1:4
        x=[x i];
    end
    for i=1:length(x)
        y=[y f(x(i))];
    end
    P1=[];
    for i=1:length(x)
        P1=[P1 Lagr(xx, yy,x(i))];
    end
    xxx=[];
    yyy=[];
    P2=[];
    for i=-2:6/(1.5*(a-1)):4
        xxx=[xxx i];
    end
    for i=1:length(xxx)
        yyy=[yyy f(xxx(i))];
    end
    for i=1:length(x)
        P2=[P2 Lagr(xxx, yyy,x(i))];
    end
    
    xxxx=[];
    yyyy=[];
    P3=[];
    for i=-2:6/(2*(a-1)):4
        xxxx=[xxxx i];
    end
    for i=1:length(xxxx)
        yyyy=[yyyy f(xxxx(i))];
    end
    for i=1:length(x)
        P3=[P3 Lagr(xxxx, yyyy,x(i))];
    end
    
    plot(x,y)
    hold on
    plot(x,P1)
    hold on
    plot(x,P2)
    hold on
    plot(x,P3)
    grid minor
    xlabel('x');
    ylabel('y')
    title('Иллюстрация работы полиномов');
    legend('функция','полином 5 степени','полином 8 степени','полином 10 степени');
    
    P0=[];
    for i=1:length(x)
        P0=[P0 NaN];
        P1(i)=abs(P1(i)-y(i));
        P2(i)=abs(P2(i)-y(i));
        P3(i)=abs(P3(i)-y(i));
    end
    figure
    plot(x,P0)
    hold on
    plot(x,P1)
    hold on
    plot(x,P2)
    hold on
    plot(x,P3)
    hold on
    grid minor
    xlabel('x');
    ylabel('y')
    title('График ошибки в каждой точке');
    legend('','полином 5 степени','полином 8 степени','полином 10 степени');
end

function [] = inter1CHE(f,a)
    xx=[];
    yy=[];

    for i=1:a
        xx=[xx (1+3*cos((2*i -1)*3.14/(2*a)))];
    end
    for i=1:length(xx)
        yy=[yy f(xx(i))];
    end
    x=[];
    y=[];
    for i=-2:0.1:4
        x=[x i];
    end
    for i=1:length(x)
        y=[y f(x(i))];
    end
    P1=[];
    for i=1:length(x)
        P1=[P1 Lagr(xx, yy,x(i))];
    end
    xxx=[];
    yyy=[];
    P2=[];
    for i=1:1.5*a
        xxx=[xxx (1+3*cos((2*i -1)*3.14/(3*a)))];
    end
    for i=1:length(xxx)
        yyy=[yyy f(xxx(i))];
    end
    for i=1:length(x)
        P2=[P2 Lagr(xxx, yyy,x(i))];
    end
    
    xxxx=[];
    yyyy=[];
    P3=[];
    for i=1:2*a
        xxxx=[xxxx (1+3*cos((2*i -1)*3.14/(4*a)))];
    end
    for i=1:length(xxxx)
        yyyy=[yyyy f(xxxx(i))];
    end
    for i=1:length(x)
        P3=[P3 Lagr(xxxx, yyyy,x(i))];
    end
    
    plot(x,y)
    hold on
    plot(x,P1)
    hold on
    plot(x,P2)
    hold on
    plot(x,P3)
    grid minor
     xlabel('x');
    ylabel('y')
    title('Иллюстрация работы полиномов');
    legend('функция','полином 5 степени','полином 8 степени','полином 10 степени');
    
    P0=[];
    for i=1:length(x)
        P0=[P0 NaN];
        P1(i)=abs(P1(i)-y(i));
        P2(i)=abs(P2(i)-y(i));
        P3(i)=abs(P3(i)-y(i));
    end
    figure
    plot(x,P0)
    hold on
    plot(x,P1)
    hold on
    plot(x,P2)
    hold on
    plot(x,P3)
    hold on
    grid minor
    xlabel('x');
    ylabel('y')
    title('График ошибки в каждой точке');
    legend('','полином 5 степени','полином 8 степени','полином 10 степени');
end


function []= inter2L(f)
    SS=[];
    for a=1:100
        xx=[];
        yy=[];

        for i=-2:6/(a-1):4
            xx=[xx i];
        end
        for i=1:length(xx)
            yy=[yy f(xx(i))];
        end
        x=[];
        y=[];
        for i=-2:0.1:4
            x=[x i];
        end
        for i=1:length(x)
            y=[y f(x(i))];
        end
         P=[];
        for i=1:length(x)
            P=[P Lagr(xx, yy,x(i))];
        end
        for i=1:length(x)
            P(i)=abs(P(i)-y(i));
        end
        S= max(abs(P));
        SS=[SS S];
    end
    A=[];
    for a=1:100
        A=[A a];
    end
    semilogy(A,SS)
    grid on
    xlabel('количество узлов');
    ylabel('ошибка')
    title('Зависимость ошибки интерполяции от количества узлов');
end

function []= inter2CHE(f)
    SS=[];
    for a=1:100
        xx=[];
        yy=[];

        for i=1:a
            xx=[xx (1+3*cos((2*i -1)*3.14/(2*a)))];
        end
        for i=1:length(xx)
            yy=[yy f(xx(i))];
        end
        x=[];
        y=[];
        for i=-2:0.1:4
            x=[x i];
        end
        for i=1:length(x)
            y=[y f(x(i))];
        end
         P=[];
        for i=1:length(x)
            P=[P Lagr(xx, yy,x(i))];
        end
        PP=[];
        for i=1:length(x)
            PP=[PP abs(P(i)-y(i))];
        end
        S= max(PP);
        SS=[SS S];
    end
    A=[];
    for a=1:100
        A=[A a];
    end
    semilogy(A,SS)
    grid on
    xlabel('количество узлов');
    ylabel('ошибка')
    title('Зависимость ошибки интерполяции от количества узлов');
end

function [] = inter3L(f,x1,x2)
    PP1=[];
    PP2=[];
    A=[];
     for a=10:100
        xx=[];
        yy=[];

        for i=-2:6/(a-1):4
            xx=[xx i];
        end
        for i=1:length(xx)
            yy=[yy f(xx(i))];
        end
        P1=Lagr(xx,yy,x1);
        P2=Lagr(xx,yy,x2);
        A1=f(x1);
        A2=f(x2);
        PP1=[PP1 abs(P1-A1)];
        PP2=[PP2 abs(P2-A2)];
        A=[A a];
     end
    semilogy(A,PP1)
    hold on
    semilogy(A,PP2)
    grid minor
    xlabel('количество узлов');
    ylabel('ошибка')
    title('Зависимость ошибки интерполяции от количества узлов в 2 точках');
end

function [] = inter3CHE(f,x1,x2)
    PP1=[];
    PP2=[];
    A=[];
     for a=10:100
        xx=[];
        yy=[];

        for i=1:a
            xx=[xx (1+3*cos((2*i -1)*3.14/(2*a)))];
        end
        for i=1:length(xx)
            yy=[yy f(xx(i))];
        end
        P1=Lagr(xx,yy,x1);
        P2=Lagr(xx,yy,x2);
        A1=f(x1);
        A2=f(x2);
        PP1=[PP1 abs(P1-A1)];
        PP2=[PP2 abs(P2-A2)];
        A=[A a];
     end
    semilogy(A,PP1)
    hold on
    semilogy(A,PP2)
    grid minor
    xlabel('количество узлов');
    ylabel('ошибка')
    title('Зависимость ошибки интерполяции от количества узлов в 2 точках');
end

function[]= inter4L(f,a)
    SS=[];
    A=[];
    for j=0:5
        xx=[];
        yy=[];
        for i=-2:6/(a-1):4
            if (rand<0.5)
                d = -1;
            else
                d=1;
            end
            xx=[xx i*(1+d*0.01*j*d+(rand*j*d*0.001))];
        end
        for i=1:length(xx)
            yy=[yy f(xx(i))];
        end
        x=[];
        y=[];
        for i=-2:0.1:4
            x=[x i];
        end
        for i=1:length(x)
            y=[y f(x(i))];
        end
         P=[];
        for i=1:length(x)
            P=[P Lagr(xx, yy,x(i))];
        end
        PP=[];
        for i=1:length(x)
            if y(i)~=0
                PP(i)=100*abs((P(i)-y(i))/y(i));
            end
        end
        
        S= max(PP);
        SS=[SS S];
        A=[A j];
    end
   
    plot(A,SS)
    grid on
    xlabel('процент возмущений исходных данных');
    ylabel('относительная погрешность')
    title('Зависимость относительной погрешности от процента возмущений исходных данных');
end

function[]= inter4CHE(f,a)
    SS=[];
    A=[];
    for j=0:5
        xx=[];
        yy=[];
        for i=1:a
            if (rand<0.5)
                d = -1;
            else
                d=1;
            end
            xx=[xx (1+3*cos((2*i -1)*3.14/(2*a)))*(1+d*0.01*j*d+(rand*j*d*0.001))];
        end
        for i=1:length(xx)
            yy=[yy f(xx(i))];
        end
        x=[];
        y=[];
        for i=-2:0.1:4
            x=[x i];
        end
        for i=1:length(x)
            y=[y f(x(i))];
        end
         P=[];
        for i=1:length(x)
            P=[P Lagr(xx, yy,x(i))];
        end
        PP=[];
        for i=1:length(x)
            if y(i)~=0
                PP(i)=100*abs((P(i)-y(i))/y(i));
            end
        end
        
        S= max(PP);
        SS=[SS S];
        A=[A j];
    end
    plot(A,SS)
    grid on
    xlabel('процент возмущений исходных данных');
    ylabel('относительная погрешность')
    title('Зависимость относительной погрешности от процента возмущений исходных данных');
end

