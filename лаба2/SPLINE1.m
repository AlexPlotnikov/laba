clc,close all, clear all;
f=@(x) x - sin(x);
ff=@(x) 1-cos(x);
%spl1(f,ff,5)%(Графики: функции, 3-х сплайнов для
             %нее,ошибка)
%spl2(f,ff)%(График ошибки от количества узлов)
%spl3(f,-2,4,ff)%(График щшибки от количества узлов в 2 точках)
%spl4(f,35,ff)%(График относительной погрешности от процента возмущений исходных данных)

function [] = spl1(f,ff,a)
    xx=[];
    yy=[];

    for i=-10:20/(a-1):10
        xx=[xx i];
    end
    for i=1:length(xx)
        yy=[yy f(xx(i))];
    end
    x=[];
    y=[];
    for i=-10:0.1:10
        x=[x i];
    end
    for i=1:length(x)
        y=[y f(x(i))];
    end
    P1=[];
    for i=1:length(x)
        P1=[P1 sp(xx, yy,x(i),ff)];
    end
    xxx=[];
    yyy=[];
    P2=[];
    for i=-10:20/(1.5*(a-1)):10
        xxx=[xxx i];
    end
    for i=1:length(xxx)
        yyy=[yyy f(xxx(i))];
    end
    for i=1:length(x)
        P2=[P2 sp(xxx, yyy,x(i),ff)];
    end
    
    xxxx=[];
    yyyy=[];
    P3=[];
    for i=-10:20/(2*(a-1)):10
        xxxx=[xxxx i];
    end
    for i=1:length(xxxx)
        yyyy=[yyyy f(xxxx(i))];
    end
    for i=1:length(x)
        P3=[P3 sp(xxxx, yyyy,x(i),ff)];
    end
    
    plot(x,y)
    hold on
    plot(x,P1)
    hold on
    plot(x,P2)
    hold on
    plot(x,P3)
    grid minor
    xlabel('x');
    ylabel('y')
    title('Иллюстрация работы сплайнов');
    legend('функция','сплайнн от 5 узлов','сплайнн от 8 узлов','сплайнн от 10 узлов');
    
    P0=[];
    for i=1:length(x)
        P0=[P0 NaN];
        P1(i)=abs(P1(i)-y(i));
        P2(i)=abs(P2(i)-y(i));
        P3(i)=abs(P3(i)-y(i));
    end
    figure
    plot(x,P0)
    hold on
    plot(x,P1)
    hold on
    plot(x,P2)
    hold on
    plot(x,P3)
    hold on
    grid minor
    xlabel('x');
    ylabel('y')
    title('График ошибки в каждой точке');
    legend('','сплайнн от 5 узлов','сплайнн от 8 узлов','сплайнн от 10 узлов');
    
end

function []= spl2(f,ff)
    SS=[];
    for a=10:100
        xx=[];
        yy=[];

        for i=-10:20/(a-1):10
            xx=[xx i];
        end
        for i=1:length(xx)
            yy=[yy f(xx(i))];
        end
        x=[];
        y=[];
        for i=-10:0.1:10
            x=[x i];
        end
        for i=1:length(x)
            y=[y f(x(i))];
        end
         P=[];
        for i=1:length(x)
            P=[P sp(xx, yy,x(i),ff)];
        end
        for i=1:length(x)
            P(i)=abs(P(i)-y(i));
        end
        S= max(abs(P));
        SS=[SS S];
    end
    A=[];
    for a=10:100
        A=[A a];
    end
    semilogy(A,SS)
    grid on
    xlabel('количество узлов');
    ylabel('ошибка')
    title('Зависимость ошибки интерполяции от количества узлов');
end

function [] = spl3(f,x1,x2,ff)
    PP1=[];
    PP2=[];
    A=[];
     for a=10:100
        xx=[];
        yy=[];

        for i=-10:20/(a-1):10
            xx=[xx i];
        end
        for i=1:length(xx)
            yy=[yy f(xx(i))];
        end
        P1=sp(xx,yy,x1,ff);
        P2=sp(xx,yy,x2,ff);
        A1=f(x1);
        A2=f(x2);
        PP1=[PP1 abs(P1-A1)];
        PP2=[PP2 abs(P2-A2)];
        A=[A a];
     end
    semilogy(A,PP1)
    hold on
    semilogy(A,PP2)
    grid minor
    xlabel('количество узлов');
    ylabel('ошибка')
    title('Зависимость ошибки интерполяции от количества узлов в 2 точках');
end

function[]= spl4(f,a,ff)
    SS=[];
    A=[];
    for j=0:5
        xx=[];
        yy=[];
        for i=-10:20/(a-1):10
            if (rand<0.5)
                d = -1;
            else
                d=1;
            end
            xx=[xx i*(1+d*0.01*j*d+(rand*j*d*0.001))];
        end
        for i=1:length(xx)
            yy=[yy f(xx(i))];
        end
        x=[];
        y=[];
        for i=-10:0.1:10
            x=[x i];
        end
        for i=1:length(x)
            y=[y f(x(i))];
        end
         P=[];
        for i=1:length(x)
            P=[P sp(xx, yy,x(i),ff)];
        end
        PP=[];
        for i=1:length(x)
            if y(i)~=0
                PP(i)=abs((P(i)-y(i))/y(i));
            end
        end
        
        S= max(PP);
        SS=[SS S];
        A=[A j];
    end
   
    plot(A,SS)
    grid on
    xlabel('процент возмущений исходных данных');
    ylabel('относительная погрешность')
    title('Зависимость относительной погрешности от процента возмущений исходных данных');
end
