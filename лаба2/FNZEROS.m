clc;
clear;

X = linspace(-1.25, 2.5, 8);
YY = @(X) 3.*sign(X).*X.^4 - 8.*X.^3 - 18.*X.^2 +6;
spline = csape(X, YY(X));
figure;
fnplt(spline);
grid on;
grid minor;
otvet = fnzeros(spline);
hold on;
aa = [];
for j = 1:length(otvet)
a = fnval(spline, otvet(j));
aa = [aa a];
end
plot(otvet, aa, '*');
xlabel('X');
ylabel('Y');
title('Сплайн и его корни');

otvet1 = fnzeros(spline, -1:0);