clc;
clear;
%Csape
X = linspace(-5, 5, 6);
f = @(x) x-sin(x);
YY = f(X);
table = csape(X, YY, 'not-a-knot');
%splinetool(X,YY);
disp(table);
table1 = fnder(table);
disp(table1)

ERRORS = [];
ERRORC = [];

for i = 2:8
YYS = [];
YYC = [];
h = pi/(2^i);
X = 0:h:pi;
H(i) = h;
YS = @(x1) sin(x1);
YC = @(x2) cos(x2);
YYS = sin(X);
YYC = cos(X);
splineS = csape(X, YYS, 'natural');
splineC = csape(X, YYC, 'natural');
t = 1;
for j = 0:pi/256:pi
YYS(t) = YS(j) - fnval(splineS, j);
YYC(t) = YC(j) - fnval(splineC, j);
t = t + 1;
end
ERRORS(i) = abs(max(YYS));
ERRORC(i) = abs(max(YYC));
end
figure;
semilogy(H, ERRORS, 'linewidth', 2);
hold on;
semilogy(H, ERRORC, 'linewidth', 2);
grid minor;
xlabel('Шаг сетки');
ylabel('Ошибка');
title('Зависимость ошибки естественного кубического сплайна и функций sinx и cosx');
legend('sin', 'cos');