clc;
clear;

%Splinetool
X = linspace(-3, 3, 8);
Y = @(X) 3.*sign(X).*X.^4 - 8.*X.^3 - 18.*X.^2 +6;
splinetool(X,Y(X));

