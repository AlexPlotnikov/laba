function [y] = Lagr(X, Y,x)
    sm=0;
    for i = 1:length(X)
        pr=1;
        for j = 1:length(X)
            if j~=i
                pr=pr*(x-X(j))/(X(i)-X(j));
            end
        end
        sm= sm + Y(i)*pr;
    end
    y=sm;
end